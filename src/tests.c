#define _DEFAULT_SOURCE

#include <stdio.h>

#include "tests.h"
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define INIT_HEAP_SIZE 80000

static struct block_header* get_block_by_address(void* data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}

void* map_pages(void const* addr, size_t length) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0 );
}

static void print_splitter() {
    printf("-----------------------\n");
}

void test_1(struct block_header* heap) {
    printf("Тест 1: обычное успешное выделение памяти \n");

    const size_t size = 500;
    void *data = _malloc(size);

    if (!data)
        err("Ошибка: malloc не выделил память \n");

    debug_heap(stdout, heap);

    if (heap->is_free)
        err("Ошибка: выделенный блок свободен \n");

    if (heap->capacity.bytes != size)
        err("Ошибка: неправильное значение capacity \n");

    printf("Тест 1 успешно пройден!:)\n");

    _free(data);
}

void test_2(struct block_header* heap) {
    printf("Тест 2: Освобождение одного блока из нескольких выделенных \n");

    void *data1 = _malloc(500);
    void *data2 = _malloc(700);

    if (data1 == NULL || data2 == NULL)
        err("Ошибка: malloc не выделил память:(");

    _free(data1);

    debug_heap(stdout, heap);

    struct block_header *block1 = get_block_by_address(data1);
    struct block_header *block2 = get_block_by_address(data2);

    if (!block1->is_free) {
        err("Ошибка: свободный блок уже занят:(\n");
    }

    if (block2->is_free) {
        err("Ошибка: выделенный блок свободен:(\n");
    }

    printf("Тест 2 успешно пройден!:)\n");

    _free(data1);
    _free(data2);
}

void test_3(struct block_header* heap) {
    printf("Тест 3: Освобождение двух блоков из нескольких выделенных\n");

    const size_t size1 = 500, size2 = 1000, size3 = 1500;

    void *data1 = _malloc(size1);
    void *data2 = _malloc(size2);
    void *data3 = _malloc(size3);

    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err("Ошибка: malloc не выделил память:(\n");
    }

    _free(data2);
    _free(data1);

    debug_heap(stdout, heap);

    struct block_header *block1 = get_block_by_address(data1);
    struct block_header *block3 = get_block_by_address(data3);

    if (!block1->is_free) {
        err("Ошибка: блок уже занят:(\n");
    }

    if (block3->is_free) {
        err("Ошибка: блок свободен:(\n");
    }


    if (block1->capacity.bytes != size1 + size2 + offsetof(struct block_header, contents)) {
        err("Ошибка: свободные блоки не связались:(\n");
    }

    printf("Тест 3 успешно пройден!:)\n");

    _free(data3);
    _free(data1);
    _free(data2);
}

void test_4(struct block_header *heap) {
    printf("Тест 4: Память закончилась, новый регион памяти расширяет старый\n");

    void *data1 = _malloc(INIT_HEAP_SIZE);
    void *data2 = _malloc(INIT_HEAP_SIZE + 1000);
    void *data3 = _malloc(2000);

    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err("Ошибка: malloc не выделил память:(\n");
    }

    _free(data3);
    _free(data2);

    debug_heap(stdout, heap);

    struct block_header *block1 = get_block_by_address(data1);
    struct block_header *block2 = get_block_by_address(data2);

    if ((uint8_t *)block1->contents + block1->capacity.bytes != (uint8_t*) block2){
        err("Ошибка: новый регион не был создан после старого:(\n");
    }

    printf("Тест 4 успешно пройден!:)\n");

    _free(data1);
    _free(data2);
    _free(data3);

}

void test_5(struct block_header *heap) {
    printf("Тест 5: Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте\n");

    void *data1 = _malloc(50000);
    if (data1 == NULL) {
        err("Ошибка: malloc не выделил память:(\n");
    }

    struct block_header *address = heap;
    while (address->next != NULL) {
        address = address->next;
    }

    map_pages((uint8_t*) address + size_from_capacity(address->capacity).bytes, 1024);

    void *data2 = _malloc(100000);

    debug_heap(stdout, heap);

    struct block_header *block2 = get_block_by_address(data2);

    if (block2 == address) {
        err("Ошибка: блок не был выделен в новом месте:(\n");
    }

    printf("Тест 5 успешно пройден!:)\n");

    _free(data1);
    _free(data2);
}


void all_tests() {
    printf("Куча начала инициализацию\n");

    struct block_header* heap = (struct block_header*) heap_init(INIT_HEAP_SIZE);

    if (heap == NULL){
        err("Кучу не удалось проинициализировать\n");
    }

    test_1(heap);
    print_splitter();
    test_2(heap);
    print_splitter();
    test_3(heap);
    print_splitter();
    test_4(heap);
    print_splitter();
    test_5(heap);

    printf("Все тесты успешно пройдены!:)\n");
}
